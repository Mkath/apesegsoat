﻿using Apeseg.Soat.Interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Apeseg.Soat.Views
{
    public partial class Respuesta : ContentPage, ICanHideBackButton
    {
        public Respuesta(int pregunta)
        {
            HideBackButton = false;
            NavigationPage.SetBackButtonTitle(this, "Atrás");
            Device.OnPlatform(Android: () => { NavigationPage.SetTitleIcon(this, "iconApeseg.png"); });
            InitializeComponent();
            RespuestaPregunta(pregunta);
        }

        public bool HideBackButton { get; set; }
        private void RespuestaPregunta(int valor)
        {
            switch (valor)
            {
                case 1:
                    PreguntaLabel.Text = Mensaje.Mensaje.PreguntaUno;
                    RespuestaLabel.Text = Mensaje.Mensaje.RespuestaUno;
                    break;
                case 2:
                    PreguntaLabel.Text = Mensaje.Mensaje.PreguntaDos;
                    RespuestaLabel.Text = Mensaje.Mensaje.RespuestaDos;
                    break;
                case 3:
                    PreguntaLabel.Text = Mensaje.Mensaje.PreguntaTres;
                    RespuestaLabel.Text = Mensaje.Mensaje.RepuestaTres;
                    break;
                case 4:
                    PreguntaLabel.Text = Mensaje.Mensaje.PreguntaCuatro;
                    RespuestaLabel.Text = Mensaje.Mensaje.RespuestaCuatro;
                    break;
                case 5:
                    PreguntaLabel.Text = Mensaje.Mensaje.PreguntaCinco;
                    RespuestaLabel.Text = Mensaje.Mensaje.RespuestaCinco;
                    break;
                case 6:
                    PreguntaLabel.Text = Mensaje.Mensaje.PreguntaSeis;
                    RespuestaLabel.Text = Mensaje.Mensaje.RespuestaSeis;
                    break;
                    //case 7:
                    //    PreguntaLabel.Text = Mensaje.Mensaje.PreguntaSiete;
                    //    RespuestaLabel.Text = Mensaje.Mensaje.RespuestaDos;
                    //    break;
                    //case 8:
                    //    PreguntaLabel.Text = Mensaje.Mensaje.PreguntaOcho;
                    //    RespuestaLabel.Text = Mensaje.Mensaje.RespuestaDos;
                    //    break;
                    //default:
                    //    PreguntaLabel.Text = Mensaje.Mensaje.PreguntaNueve;
                    //    RespuestaLabel.Text = Mensaje.Mensaje.RespuestaDos;
                    //break;
            }
        }
    }
}
