﻿using Apeseg.Soat.Controls;
using Apeseg.Soat.Interfaz;
using Apeseg.Soat.Model;
using Apeseg.Soat.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Plugin.Connectivity;
//using Plugin.Connectivity.Abstractions;

using Xamarin.Forms;

namespace Apeseg.Soat.Views
{
    public partial class ConsultaSoat : ContentPage, ICanHideBackButton
    {
        public  View _layout;
        public ConsultaSoat()
        {
 
            HideBackButton = true;
            //NavigationPage.SetBackButtonTitle(this, "Atrás");
            Device.OnPlatform(Android: () => { NavigationPage.SetTitleIcon(this, "iconApeseg.png"); });

            NavigationPage.SetHasBackButton(this, false);
            InitializeComponent();
            var DosRecognizer = new TapGestureRecognizer();
            DosRecognizer.Tapped += SearchButton_Clicked;
            SearchButton.GestureRecognizers.Add(DosRecognizer);

        }

        public bool HideBackButton { get; set; }

        private void PreguntaButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Preguntas(), true);
        }

        private async void SearchButton_Clicked(object sender, EventArgs e)
        {
            //SearchButton.IsEnabled = false;
            string placa = "";
            if (Buscar.Text == "" || Buscar.Text== null)
            {
                DisplayAlert("Consultar SOAT", "Debe ingresar un número de placa", "OK");
                //  MessageBox.Show(LayoutRoot, "Debe ingresar un número de placa.");
                return;
            }




            placa = Buscar.Text.Trim().ToString();


            SoatServices oSoatServices = new SoatServices();

            //bloquear();
            //BloquearPantalla.Show(LayoutRoot, "Debe ingresar un número de placa.");
            BusyFrame.IsVisible = true;


            Certificado item = await oSoatServices.BuscarCertificadoPorPlaca(placa);

            //Valida Conexion
            if (item.ErrorConexion)
            {
                DisplayAlert("Consultar SOAT", "Actualmente no dispone de conexión a Internet", "OK");
                //SearchButton.IsEnabled = true;
                BusyFrame.IsVisible = false;
                return;
            }
            //Valida Placa
            if (item.Existe == false)
            {
                DisplayAlert("Consultar SOAT", "El vehiculo con placa " + placa + " no cuenta con un certificado de SOAT", "OK");
                //SearchButton.IsEnabled = true;
                BusyFrame.IsVisible = false;
                return;
            }

            if (item != null)
            {
                //Desbloquear();
                Buscar.Text = "";

                //var nav = new NavigationPage
                //{
                //    Title = "Detail",
                    
                    
                //};
             
                //nav.PushAsync(new ContentPage() { Title = "Home" });

                Navigation.PushAsync(new Detalle(item) );
            }
            //SearchButton.IsEnabled = true ;
            BusyFrame.IsVisible = false;
            //BloquearPantalla.Show(LayoutRoot, "");




        }


    }
}
