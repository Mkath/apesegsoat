﻿using Apeseg.Soat.Interfaz;
using Apeseg.Soat.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Apeseg.Soat.Views
{
    public partial class Detalle : ContentPage, ICanHideBackButton
    {
        public Detalle(Certificado _Certificado)
        {
            HideBackButton = false;
            //NavigationPage.SetBackButtonTitle(this, "Atrás");
            Device.OnPlatform(Android: () => { NavigationPage.SetTitleIcon(this, "iconApeseg.png"); });

            //Device.OnPlatform(iOS: () => { NavigationPage.SetTitleIcon(this, "iconApeseg.png"); });
            Device.OnPlatform(iOS: () => { NavigationPage.SetBackButtonTitle(this, "Atras"); });
            InitializeComponent();
            ObtenerDetalle(_Certificado);

            var tapImage = new TapGestureRecognizer();
            //Binding events  
            tapImage.Tapped += PreguntaButton_Clicked;
            //Associating tap events to the image buttons  
            PreguntaButton.GestureRecognizers.Add(tapImage);

            var DosRecognizer = new TapGestureRecognizer();
            DosRecognizer.Tapped += CallButton_Clicked;
            CallButton.GestureRecognizers.Add(DosRecognizer);


        }
        public bool HideBackButton { get; set; }
        private void CallButton_Clicked(object sender, EventArgs e)
        {
            try
            {
                ICallService callService = DependencyService.Get<ICallService>();
                callService.MakeCall("01" + NumeroAseguradoraEntry.Text);
            }
            catch (Exception ex)
            {
                DisplayAlert("Consultar SOAT", "Error", "Error");
            }

        }

        private void PreguntaButton_Clicked(object sender, EventArgs e)
        {
            //BusyFrame.IsVisible = true;
            //if (PreguntaButton.IsEnabled)
            //{
                Navigation.PushAsync(new Preguntas(), true);
                //PreguntaButton.IsEnabled = false;
            //}
            
        }

        private void ObtenerDetalle(Certificado _Certificado)
        {
            PlacaEntry.Text = _Certificado.Placa;
            CompaniaEntry.Text = _Certificado.NombreCompania.ToUpper();
            PolizaEntry.Text = _Certificado.NumeroPoliza;
            VigenciaAnualEntry.Text = _Certificado.FechaInicio + " - " + _Certificado.FechaFin;
            VigenciaControlEntry.Text = _Certificado.FechaInicio + " - " + _Certificado.FechaFin;
            UsoEntry.Text = _Certificado.NombreUsoVehiculo.ToUpper();
            //ClaseEntry.Text = _Certificado.NombreClaseVehiculo.ToUpper();
            EstadoEntry.Text = _Certificado.Estado;
            TipoCertificadoEntry.Text = _Certificado.TipoCertificado;
            NumeroAseguradoraEntry.Text = (_Certificado.NumeroAseguradora).Remove(0, 3);

        }
    }
}
