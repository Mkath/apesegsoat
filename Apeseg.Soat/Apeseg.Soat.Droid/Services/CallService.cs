using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Apeseg.Soat.Interfaz;
using System.Text.RegularExpressions;
using Xamarin.Forms;
using Uri = Android.Net.Uri;
using Apeseg.Soat.DroidTest.Services;

[assembly: Dependency(typeof(CallService))]
namespace Apeseg.Soat.DroidTest.Services
{
    class CallService : ICallService
    {
        public static void Init()
        {

        }
        public void MakeCall(string phone)
        {
            if (Regex.IsMatch(phone, "^(\\(?\\+?[0-9]*\\)?)?[0-9_\\- \\(\\)]*$"))
            {
                var uri = Uri.Parse(String.Format("tel:{0}", phone));
                var intent = new Intent(Intent.ActionView, uri);
                Forms.Context.StartActivity(intent);
            }
            else
            {
                
            }
        }
    }
}